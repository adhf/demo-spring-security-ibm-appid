package com.example.demo;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DEFController {

	@GetMapping("/abc")
	@Secured("ROLE_ADMIN")
	public String abc() {
		return "Hello abc with role ROLE_ADMIN";
	}

    @GetMapping("/def")
	@Secured("ROLE_USER")
	public String def() {
		return "Hello def with role ROLE_USER";
	}

	@GetMapping("/ghi")
	public String ghi() {
		return "Hello ghi";
	}

}
